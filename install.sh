#!/bin/sh

set -e

DEST=${1:-/opt/flac-metadata}

if [ 0 -ne $(id -u) ]; then
    echo "Need to be root"
    exit 1
fi

if [ ! -d ${DEST} ]; then
    mkdir ${DEST}
fi

cp -r flacmetadata/common flacmetadata/*.py ${DEST}

cat << EOF > /usr/local/bin/flacmetadata
#!/bin/sh

python3 /opt/flac-metadata/metadata.py \$@
EOF

chmod +x /usr/local/bin/flacmetadata

cat << EOF > /usr/local/bin/flacrename
#!/bin/sh

python3 /opt/flac-metadata/rename.py \$@
EOF

chmod +x /usr/local/bin/flacrename
