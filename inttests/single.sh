#!/bin/bash

set -ex

test -d "flacmetadata/test/flacs/album"
test ! -d "flacmetadata/test/flacs/Artist - 2000 - Album"
test -e "flacmetadata/test/flacs/album/paprika.jpg"
test -e "flacmetadata/test/flacs/album/01 - Title 01.flac"
test -e "flacmetadata/test/flacs/album/02 - Title 02.flac"
test -e "flacmetadata/test/flacs/album/03 - Title 03.flac"
test -e "flacmetadata/test/flacs/album/04 - Title 04.flac"
test -e "flacmetadata/test/flacs/album/05 - Title 05.flac"

python3 flacmetadata/rename.py -v flacmetadata/test/flacs/album

test -d "flacmetadata/test/flacs/Artist - 2000 - Album"
test ! -d "flacmetadata/test/flacs/album"

test -e "flacmetadata/test/flacs/Artist - 2000 - Album/cover.jpg"
test -e "flacmetadata/test/flacs/Artist - 2000 - Album/01 - Artist - Title 01.flac"
test -e "flacmetadata/test/flacs/Artist - 2000 - Album/02 - Artist - Title 02.flac"
test -e "flacmetadata/test/flacs/Artist - 2000 - Album/03 - Artist - Title 03.flac"
test -e "flacmetadata/test/flacs/Artist - 2000 - Album/04 - Artist - Title 04.flac"
test -e "flacmetadata/test/flacs/Artist - 2000 - Album/05 - Artist - Title 05.flac"

command -v git && git checkout flacmetadata/test/flacs/ && rm -rf flacmetadata/test/flacs/Artist\ -\ 2000\ -\ Album/ || true

