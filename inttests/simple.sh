#!/bin/bash

python3 flacmetadata/metadata.py read .
python3 flacmetadata/metadata.py read -t a -t t .
python3 flacmetadata/metadata.py missing -t a flacmetadata/test/flacs/album
python3 flacmetadata/metadata.py uniform flacmetadata/test/flacs/album
