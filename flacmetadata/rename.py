#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import argparse
import subprocess
import string
import re
import os

import imghdr

from common.metaflac import fetch_metadata, set_metadata, expand_tag, expand_tags, TAGS


# Chars that are not going to be used in filenames. 
UNWANTEND_CHARS = r'[/\!?%$*|"\'<>]'


class Action:
    """ Accumulates all the actions that are needed to be performed to rename the target files. """
    def __init__(self, directory):
        self.dir = directory
        self.file_actions = dict()
        # dir_action denotes the dir that is to be renamed. the first entry of the tuple is the old directory
        # the second argument is the new directory.
        self.dir_action = tuple()
        # image_action's first tuple entry is the path of the old image, the 2nd entry the path to the new
        # image file.
        self.image_action = tuple()
        self.errors = list()

    def add_error(self, error):
        self.errors.append(error)

    def add_file_action(self, old_filepath, new_filepath):
        """ Add an action to rename a file. """
        self.file_actions[old_filepath] = new_filepath

    def set_dir_action(self, old_filepath, new_filepath):
        """ Add an action to rename a directory. """
        self.dir_action = (old_filepath, new_filepath)

    def set_image_action(self, old_filepath, new_filepath):
        """ Add an action to rename a image file. """
        self.image_action = (old_filepath, new_filepath)

    def actionable(self):
        """
        Returns whether there is work to do or not. Returns True, if there are actions to perform,
        otherwise False.
        """
        return bool(self.file_actions or self.dir_action)

    def encountered_errors(self):
        """ Returns the errors encountered while renaming the files. """
        return len(self.errors) > 0

    def carry_out(self, dryrun=False, verbose=False):
        """ Actually perform the operations. """
        if self.file_actions:
            for key in self.file_actions:
                if not dryrun:
                    os.rename(key, self.file_actions[key])
                if verbose:
                    print(f'"{key}" --> "{self.file_actions[key]}"')

        if self.image_action:
            if not dryrun:
                os.rename(self.image_action[0], self.image_action[1])
            if verbose:
                print(f'"{self.image_action[0]}" --> "{self.image_action[1]}"')

        if self.dir_action:
            if not dryrun:
                    os.rename(self.dir_action[0], self.dir_action[1])
            if verbose:
                print(f'"{self.dir_action[0]}" --> "{self.dir_action[1]}"')

    def __repr__(self):
        ret = f"{self.dir}: \n"
        if not self.dir_action and not self.file_actions:
            return "\tNothing to do"

        if self.dir_action:
            ret += f"Dir action: \n\t{self.dir_action[0]} -> {self.dir_action[1]}\n\n"

        if self.image_action:
            ret += f"Image action: \n\t{self.image_action[0]} -> {self.image_action[1]}\n\n"

        if self.file_actions:
            ret += "File action(s): \n"
            for old in self.file_actions:
                ret += f"\t- {old} -> {self.file_actions[old]}\n"
        return ret

def unwrap_keys(scheme, directory=False):
    """Process the scheming arguments."""
    if not directory and not re.search("%[tn]", scheme):  # To have a unique filename
        sys.exit("Error: %t or %n has to be present in <scheme>")
    scheme = re.sub('%%([%s])' % ''.join(TAGS.keys()),
                    lambda m: '%%(%s)s' % TAGS[m.group(1)],
                    scheme)
    return scheme

def has_sufficient_metadata(metadata, scheme):
    """
    Checks whether the supplied metadata is enough to perform the rename operation.
    Returns True, if there is enough metadata, otherwise False.
    """
    if not metadata or not scheme:
        return False

    tags = re.findall(r"\(\w+\)", scheme)
    for tag in tags:
        # cut the parantheses from the tag
        tag = tag[1:-1]
        if not tag in metadata or not metadata[tag]:
            return False

    return True


def rename_file(scheme, dirname, filename, metadata):
    """ Renames a file """
    filepath = os.path.join(dirname, filename)
    new_filename = scheme % metadata + ".flac"
    new_filename = re.sub(UNWANTEND_CHARS, '', new_filename)
    new_filepath = os.path.join(dirname, new_filename)

    if filepath == new_filepath:
        return None

    return filepath, new_filepath

def rename_dir(dir_scheme, dirname, file_metadata):
    """
    Renames the directory that contains the music to the desired scheme
    """
    path = os.path.dirname(dirname)
    new_dirname = dir_scheme % file_metadata
    new_dirname = re.sub(UNWANTEND_CHARS, '', new_dirname)
    new_filepath = os.path.join(path, new_dirname)

    if dirname == new_filepath:
        return None

    return dirname, new_filepath

def parse_args():
    """
    Parses the arguments given to the program.
    :return: parsed Namespace with the arguments.
    """
    parser = argparse.ArgumentParser(prog='flac-renamer')
    parser.add_argument('-s', '--scheme', dest='scheme', action="store", default="%n - %a - %t")
    parser.add_argument('-ds', '--directory-scheme', dest='dir_scheme', action='store', default='%a - %d - %b')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', default=False)
    parser.add_argument('-n', '--dry-run', dest='dryrun', action='store_true', default=False)
    parser.add_argument('-c', '--cover-name', dest='covername', action='store', default="cover")
    parser.add_argument('target', action="store")

    return parser.parse_args()

def append_metadata(album_metadata, file_metadata):
    """
    Appends metadata of a flac file to a dict that holds metadata of an album/disc/compilation.
    """
    for key in file_metadata.keys():
        if not key in album_metadata:
            album_metadata[key] = set()
        album_metadata[key].add(file_metadata[key])


def can_rename_directory(metadata, scheme):
    """
    Decides based on the given metadata whether the directory can be renamed or not.
    """
    if not metadata:
        return False

    tags = re.findall(r"\(\w+\)", scheme)
    for tag in tags:
        # cut the parantheses from the tag
        tag = tag[1:-1]
        # check if key is existent in dict
        # and the set is not None
        # and only one element is in it
        # and that element is not empty
        if  not tag in metadata or \
            not metadata[tag] or \
            len(metadata[tag]) != 1 or \
            not max(metadata[tag]):
            return False
    
    return True

def get_main_cover(dirname, images):
    """
    Accepts a list of images and tries to guess which is the correct file to use as the cover.
    """
    if not images:
        return None

    # if there is only a single image, return that.
    if len(images) == 1:
        return images[0]

    # if there is a image whose name indicates it's a cover, return that.
    for image in images:
        if  "front" in image.lower() or \
            "folder" in image.lower() or \
            "folder" in image.lower() or \
            "devant" in image.lower():
            return image

    # everything else failed – just pick the image with the biggest file size.
    return max(images, key=(lambda img: os.path.getsize(os.path.join(dirname, img))))


def get_extension(filename):
    """
    Returns the extension of a filename.
    """
    extension = os.path.splitext(filename)
    if len(extension) >= 2:
        return extension[1]

    return ""

def rename_cover(dirname, images, args):
    """
    Renames a given image to the desired name. 
    """
    selected_image = get_main_cover(dirname, images)
    old_filepath = os.path.join(dirname, selected_image)
    new_filename = args.covername + get_extension(selected_image)
    new_filepath = os.path.join(dirname, new_filename)
    if old_filepath == new_filepath:
        return None

    return old_filepath, new_filepath

def is_image(dirname, filename):
    """
    Returns True if the submitted file name indicates being an image, otherwise False.
    """
    filepath = os.path.join(dirname, filename)
    return imghdr.what(filepath) is not None


def fix_path(args):
    """ Makes sure the path does not end with a trailing slash. """
    if args.target.endswith("/"):
        args.target = args.target[:-1]

def work_dir(dirname, filenames, file_scheme, dir_scheme, args):
    album_metadata = dict()
    collected_images = list()
    file_metadata = None
    dir_contains_music = False

    action = Action(dirname)

    for filename in sorted(filenames):
        if filename.lower().endswith('.flac'):
            dir_contains_music = True
            try:
                filepath = os.path.join(dirname, filename)
                file_metadata = fetch_metadata(filepath)
                if not has_sufficient_metadata(file_metadata, file_scheme):
                    action.add_error(f"No metadata for {filename}")
                else:
                    ret = rename_file(file_scheme, dirname, filename, file_metadata)
                    if ret:
                        action.add_file_action(ret[0], ret[1])
                    append_metadata(album_metadata, file_metadata)
            except KeyboardInterrupt:
                raise
            except OSError as e:
                action.add_error(f"Error: {str(e)}")

        elif is_image(dirname, filename):
            collected_images.append(filename)
    
    if dir_contains_music:
        if collected_images:
            ret = rename_cover(dirname, collected_images, args)
            if ret:
                action.set_image_action(ret[0], ret[1])

        if can_rename_directory(album_metadata, dir_scheme):
            ret = rename_dir(dir_scheme, dirname, file_metadata)
            if ret:
                action.set_dir_action(ret[0], ret[1])
        else:
            action.add_error(f"Can not rename {dirname} due to missing metadata.")

    return action

def work(args):
    file_scheme = unwrap_keys(args.scheme)
    dir_scheme = unwrap_keys(args.dir_scheme, directory=True)

    if not os.path.isdir(args.target):
        sys.exit(f"Error: {args.target} is not a valid directory")

    fix_path(args)
    exit_code = 0
    for dirname, _, filenames in os.walk(args.target, topdown=False):
        action = work_dir(dirname, filenames, file_scheme, dir_scheme, args)
        if action and action.actionable():
            try:
                action.carry_out(args.dryrun, args.verbose)
            except Exception as error:
                print(f"Error while applying changes: {str(error)}")
                exit_code = 1
        if action.encountered_errors():
            print(f'Errors encountered: {action.errors}')
            exit_code = 1

    sys.exit(exit_code)

def main():
    """Main function."""
    args = parse_args()
    work(args)


if __name__ == "__main__":
    main()
