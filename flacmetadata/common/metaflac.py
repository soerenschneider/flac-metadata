import subprocess

# flac metadata tags
TAGS = dict(a='artist', b='album', c='composer', d='date', g='genre', n='tracknumber', t='title')

def fetch_metadata(filepath, tags=None):
    """
    Fetches metadata for a given file path. The metadata is returned as a string/string dict.
    """
    if not tags:
        tags = TAGS.values()

    args = ["--show-tag=%s" % tag for tag in tags]

    # make sure the keys exist. Prevents failure in case of incomplete metadata
    pipe = subprocess.Popen(["metaflac"] + args + [filepath], stdout=subprocess.PIPE)
    output, error = pipe.communicate()
    if pipe.returncode:
        raise IOError("metaflac failed: %s" % error)
    output = output.decode('utf-8') # binary to string
    output = output.splitlines()

    metadata = dict()
    for item in output:
        if "=" not in item:
            continue

        index = item.index("=")
        value = item[index+1:]
        if not value:
            continue

        tag = item[:index].lower()
        if tag == "tracknumber":
            metadata[tag] = value.zfill(2)
        else:
            metadata[tag] = value

    # attach metadata to the metadata
    if len(metadata) > 0:
        metadata["_filepath"] = filepath

    return metadata


def set_metadata(filepath, tag, value):
    """
    Write / overwrite a metadata value for a given file.
    """
    if not tag:
        return

    tag = tag.upper()

    args = f"--remove-tag={tag}"
    pipe = subprocess.Popen(["metaflac"] + [args] + [filepath], stdout=subprocess.PIPE)
    output, error = pipe.communicate()
    if pipe.returncode:
        raise IOError("metaflac failed: %s" % error)

    args = f"--set-tag={tag}={value}"
    pipe = subprocess.Popen(["metaflac"] + [args] + [filepath], stdout=subprocess.PIPE)
    output, error = pipe.communicate()
    if pipe.returncode:
        raise IOError("metaflac failed: %s" % error)

def expand_tag(tag):
    """
    Expands the given tag from the short notation to the long notation.
    Returns the string representation of the tag.
    """
    if not tag:
        return None

    tag = tag.lower()
    if tag not in TAGS.keys():
        return tag

    return TAGS[tag]

def expand_tags(tags):
    """
    Expands the given tags from their short notation to the long notation.
    Returns a list of strings for the tags.
    """
    if not tags:
        return []

    ret = list()
    for tag in tags:
        expanded = expand_tag(tag)
        if expanded:
            ret.append(expanded)
    return ret
