#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from common.metaflac import fetch_metadata, set_metadata, expand_tag, expand_tags, TAGS

import argparse
import subprocess
import string
import re
import os


# list of accepted tags for parsing
ACCEPTED_TAGS = list()
ACCEPTED_TAGS.extend([x for x in TAGS.keys()])
ACCEPTED_TAGS.extend([x for x in TAGS.values()])

subcmd_missing = "missing"
subcmd_uniform = "uniform"

def parse_args():
    """
    Parses the arguments given to the program.
    :return: parsed Namespace with the arguments.
    """
    parser = argparse.ArgumentParser(prog='flac-metadata')
    subparsers = parser.add_subparsers(dest='subparser')

    parser_get = subparsers.add_parser('read', help='Reads metadata for all flac files under target. If no explicit tags are supplied, all tags may be printed.')
    parser_get.add_argument('-t', '--tag', dest='tags', action='append', choices=ACCEPTED_TAGS)
    parser_get.add_argument('target', action='store')

    parser_set = subparsers.add_parser('write', help='Writes metadata for a single tag for all flac files in target. A tag and a value must be supplied. To delete a tag, supply an empty string as value.')
    parser_set.add_argument('-t', '--tag', dest='tag', action='store', required=True, choices=ACCEPTED_TAGS)
    parser_set.add_argument('-v', '--value', dest='value', action='store', required=True)
    parser_set.add_argument('target', action='store')

    parser_uniform = subparsers.add_parser(subcmd_uniform, help='Make sure that the supplied tags have only a single value across all files under the target. If no tags are explicitely supplied, [artist, album, date, genre] is used.')
    parser_uniform.add_argument('-t', '--tags', dest='tags', action='append', choices=ACCEPTED_TAGS)
    parser_uniform.add_argument('target', action='store')

    parser_missing = subparsers.add_parser(subcmd_missing, help='Determines whether tags are missing. If no tags are supplied, [artist, album, date, genre, tracknumber, title] is implicitely used. ')
    parser_missing.add_argument('-t', '--tags', dest='tags', action='append', choices=ACCEPTED_TAGS)
    parser_missing.add_argument('target', action='store')

    return parser.parse_args()


def pprint_metadata(metadata):
    """ Pretty print dict of metadata. """
    if not metadata:
        return

    for line in metadata:
        keys = len(metadata.keys())
        index = 0
        for key in metadata.keys():
            # ignore meta-metadata :)
            if key[0] != "_":
                index += 1
                print(f'{key} -> "{metadata[key]}"', end='')
                if index < keys:
                    print('; ', end='')
        print("")

def read_metadata(target, tags):
    read_metadata = list()
    if not os.path.isdir(target):
        metadata = fetch_metadata(target, tags)
        pprint_metadata(metadata)
    else:
        for dirname, _, filenames in os.walk(target, topdown=False):
            announced_dirname = False
            for filename in sorted(filenames):
                if filename.lower().endswith('.flac'):
                    filepath = os.path.join(dirname, filename)
                    metadata = fetch_metadata(filepath, tags)

                    if not announced_dirname:
                        print(dirname)
                        announced_dirname = True
                    pprint_metadata(metadata)

def write_metadata(target, tag, value):
    """ Writes metadata to the collected files. """
    if not os.path.isdir(target):
        set_metadata(target, tag, value)
    else:
        for dirname, _, filenames in os.walk(target, topdown=False):
            for filename in sorted(filenames):
                if filename.lower().endswith('.flac'):
                    filepath = os.path.join(dirname, filename)
                    set_metadata(filepath, tag, value)

def attach_metadata(album_metadata, file_metadata):
    """ Convenience function that attaches metadata of a file to a dict of metadata. """
    for key in file_metadata.keys():
        if key not in album_metadata:
            album_metadata[key] = set()
        album_metadata[key].add(file_metadata[key])

def get_missing_tags(album_metadata, tags):
    """ Returns tags that are wanted by the user but are not present in the collected metadata. """
    ret = list()
    for tag in tags:
        if tag not in album_metadata or not album_metadata[tag]:
            ret.append(tag)

    return ret

def get_multi_valued_keys(collected_metadata, tags):
    """ Returns tags that have multiple values across the collected metadata. """
    ret = list()
    for tag in tags:
        if tag in collected_metadata and len(collected_metadata[tag]) > 1:
            ret.append(tag)

    return ret

def collect_metadata(target, tags=None):
    """ Reads and aggregates all metadata from the target. Metadata is returned as dict. """
    collected_metadata = dict()

    if not target:
        return collected_metadata

    if not os.path.isdir(target):
        file_metadata = fetch_metadata(target, tags)
        attach_metadata(collected_metadata, file_metadata)

    for dirname, _, filenames in os.walk(target, topdown=False):
        for filename in sorted(filenames):
            if filename.lower().endswith('.flac'):
                filepath = os.path.join(dirname, filename)
                file_metadata = fetch_metadata(filepath, tags)
                attach_metadata(collected_metadata, file_metadata)

    return collected_metadata

def uniform(target, tags):
    collected_metadata = collect_metadata(target, tags)

    missing(target, tags)

    multiply_occuring_tags = get_multi_valued_keys(collected_metadata, tags)
    if multiply_occuring_tags:
        print(f'E: Values for tags {multiply_occuring_tags} not uniform.')
        for tag in multiply_occuring_tags:
            print(f'{tag} -> {collected_metadata[tag]}')
        sys.exit(1)


def missing(target, tags):
    collected_metadata = collect_metadata(target, tags)
    missing_tags = get_missing_tags(collected_metadata, tags)
    if missing_tags:
        print(f'E: missing tags: {missing_tags}')
        sys.exit(1)


def main():
    """Main function."""
    args = parse_args()

    if not args.subparser:
        sys.exit("You must specify a subcommand. ")

    if not os.path.exists(args.target):
        sys.exit(f'Specified target "{args.target}" does not exist. ')

    if args.subparser == 'read':
        args.tags = expand_tags(args.tags)
        read_metadata(args.target, args.tags)
    elif args.subparser == 'write':
        args.tag = expand_tag(args.tag)
        write_metadata(args.target, args.tag, args.value)
    elif args.subparser == subcmd_missing:
        if not args.tags:
            args.tags = ['artist', 'album', 'date', 'genre', 'tracknumber', 'title']
        args.tags = expand_tags(args.tags)
        missing(args.target, args.tags)
    elif args.subparser == subcmd_uniform:
        if not args.tags:
            args.tags = ['artist', 'album', 'date', 'genre']
        args.tags = expand_tags(args.tags)
        uniform(args.target, args.tags)

if __name__ == "__main__":
    main()
