from rename import *
import unittest   # The test framework
import os
import re
import argparse

HERE = os.path.abspath(os.path.dirname(__file__))
populated_path = os.path.join(HERE, "flacs/tests_populated.flac")
blank_path = os.path.join(HERE, "flacs/tests_blank.flac")

class Test_TestIncrementDecrement(unittest.TestCase):

    def test_is_sufficient_negative(self):
        metadata = fetch_metadata(blank_path)
        assert not has_sufficient_metadata(metadata, "%(tracknumber)s - %(artist)s - %(title)s")


    def test_renaming(self):
        flacs = os.path.join(HERE, "flacs/album")
        filenames = ['01 - Title 01.flac', '02 - Title 02.flac', '03 - Title 03.flac', '04 - Title 04.flac', '05 - Title 05.flac']
        args = argparse.Namespace()
        action = work_dir(flacs, filenames, r"%(tracknumber)s - %(artist)s - %(title)s", r"%(artist)s - %(date)s - %(album)s", args)

        assert len(action.errors) == 0

        assert action.actionable() is True
        assert len(action.file_actions) == 5

        re_old = re.compile(".*\d{2} - Title \d{2}\.flac$")
        re_new = re.compile(".*\d{2} - Artist - Title \d{2}\.flac$")
        for key in action.file_actions:
            assert re_old.match(key) 
            assert re_new.match(action.file_actions[key])

        assert len(action.dir_action) == 2
        assert re.match(".*Artist - \d{4} - Album$", action.dir_action[1])

        assert not action.image_action


    def test_is_sufficient_positive(self):
        metadata = fetch_metadata(populated_path)
        assert has_sufficient_metadata(metadata, "%(tracknumber)s - %(artist)s - %(title)s")


    def test_read_metadata_non_existent(self):
        try:
            metadata = fetch_metadata(".", "saoughasdoigjsag")
            self.fail("Expected error")
        except OSError:
            pass

    def test_can_rename_album_no_artist(self):
        album_metadata = dict()
        album_metadata['artist'] = set()
        album_metadata['date'] = set()
        album_metadata['date'].add('2017')
        album_metadata['album'] = set()
        album_metadata['album'].add('Album')

        assert not can_rename_directory(album_metadata, "%(artist)s - %(date)s - %(album)s")


    def test_can_rename_album(self):
        album_metadata = dict()
        album_metadata['artist'] = set()
        album_metadata['artist'].add('Artist')
        album_metadata['date'] = set()
        album_metadata['date'].add('2017')
        album_metadata['album'] = set()
        album_metadata['album'].add('Album')

        assert can_rename_directory(album_metadata, "%(artist)s - %(date)s - %(album)s")
                
    def test_can_rename_album_multiple_artists(self):
        album_metadata = dict()
        album_metadata['artist'] = set()
        album_metadata['artist'].add('Artist')
        album_metadata['artist'].add('Artist 2')
        album_metadata['date'] = set()
        album_metadata['date'].add('2017')
        album_metadata['album'] = set()
        album_metadata['album'].add('Album')

        assert not can_rename_directory(album_metadata, "%(artist)s - %(date)s - %(album)s")


    def test_can_rename_album_empty_artists(self):
        album_metadata = dict()
        album_metadata['artist'] = set()
        album_metadata['artist'].add('')
        album_metadata['date'] = set()
        album_metadata['date'].add('2017')
        album_metadata['album'] = set()
        album_metadata['album'].add('Album')

        assert not can_rename_directory(album_metadata, "%(artist)s - %(date)s - %(album)s")

    def test_can_rename_album_no_artist(self):
        album_metadata = dict()
        album_metadata['date'] = set()
        album_metadata['date'].add('2017')
        album_metadata['album'] = set()
        album_metadata['album'].add('Album')

        assert not can_rename_directory(album_metadata, "%(artist)s - %(date)s - %(album)s")

if __name__ == '__main__':
    unittest.main()