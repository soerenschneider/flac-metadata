from metadata import *
import unittest
import shutil
import os

from metadata import collect_metadata, get_multi_valued_keys

HERE = os.path.abspath(os.path.dirname(__file__))
populated_path = os.path.join(HERE, "flacs/tests_populated.flac")
blank_path = os.path.join(HERE, "flacs/tests_blank.flac")
album_path = os.path.join(HERE, "flacs/album")

class Test_Case(unittest.TestCase):
    def test_collect_metadata_album(self):
        collected_metadata = collect_metadata(album_path, None)
        self.assertEqual(5, len(collected_metadata['tracknumber']))
        
    def test_collect_metadata_single(self):
        collected_metadata = collect_metadata(populated_path, None)
        self.assertEqual(1, len(collected_metadata['tracknumber']))
    
    def test_multi_valued_keys_multiple(self):
        collected_metadata = collect_metadata(album_path, None)
        multi_valued = get_multi_valued_keys(collected_metadata, ["tracknumber"])
        self.assertEqual(1, len(multi_valued))
        self.assertEqual("tracknumber", multi_valued[0])

    def test_multi_valued_keys_no_multis_artist(self):
        collected_metadata = collect_metadata(album_path, None)
        multi_valued = get_multi_valued_keys(collected_metadata, ["artist"])
        self.assertEqual(0, len(multi_valued))

    def test_multi_valued_keys_no_multis_artist_albums(self):
        collected_metadata = collect_metadata(album_path, None)
        multi_valued = get_multi_valued_keys(collected_metadata, ["artist", "album"])
        self.assertEqual(0, len(multi_valued))


if __name__ == '__main__':
    unittest.main()