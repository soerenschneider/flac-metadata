from common.metaflac import fetch_metadata
from common.metaflac import set_metadata
import unittest
import shutil
import os

HERE = os.path.abspath(os.path.dirname(__file__))
populated_path = os.path.join(HERE, "flacs/tests_populated.flac")
blank_path = os.path.join(HERE, "flacs/tests_blank.flac")
dest_path = os.path.join(HERE, "flacs/tests_tmp.flac")

class Test_Common(unittest.TestCase):
    def test_fetch_metadata_single(self):
        metadata = fetch_metadata(populated_path, ['artist'])
        assert len(metadata) == 2
        assert metadata['artist'] == 'Artist'

    def test_fetch_metadata_all(self):
        metadata = fetch_metadata(populated_path)
        assert len(metadata) == 8
        assert metadata['artist'] == 'Artist'
        assert metadata['album'] == 'Album'
        assert metadata['genre'] == 'Genre'
        assert metadata['date'] == '2000'
        assert metadata['tracknumber'] == '01'
        assert metadata['title'] == 'Title'
        assert metadata['_filepath'] == populated_path

    def test_read_metadata_empty(self):
        metadata = fetch_metadata(blank_path)
        assert len(metadata) == 0

    def test_fetch_metadata_invalid(self):
        metadata = fetch_metadata(populated_path, ['bla'])
        assert len(metadata) == 0

    def test_fetch_metadata_blank_single(self):
        metadata = fetch_metadata(blank_path, ['artist'])
        assert len(metadata) == 0

    def test_fetch_metadata_blank_all(self):
        metadata = fetch_metadata(blank_path)
        assert len(metadata) == 0

    def test_fetch_metadata_blank_invalid(self):
        metadata = fetch_metadata(blank_path, ['bla'])
        assert len(metadata) == 0


    def test_set_metadata_set_value(self):
        shutil.copyfile(blank_path, dest_path)

        metadata = None
        try:
            set_metadata(dest_path, "artist", "Hello")
            metadata = fetch_metadata(dest_path)
        except Exception as e:
            print(f'Received error: {e}')

        os.remove(dest_path)
        self.assertEqual(2, len(metadata))

        assert metadata['artist'] == "Hello"


    def test_set_metadata_overwrite_value(self):
        dest = f"{HERE}/flacs/tests_set.flac"
        shutil.copyfile(populated_path, dest)

        value = "Hello"
        metadata = None
        try:
            set_metadata(f"{HERE}/flacs/tests_set.flac", "artist", value)
            metadata = fetch_metadata(f"{HERE}/flacs/tests_set.flac")
        except Exception as e:
            print(f'Received error: {e}')

        os.remove(dest)
        self.assertEqual(8, len(metadata))
        assert metadata['artist'] == value


    def test_set_metadata_delete_value(self):
        dest = f"{HERE}/flacs/tests_set.flac"
        shutil.copyfile(populated_path, dest)

        value = ""
        metadata = None
        try:
            set_metadata(f"{HERE}/flacs/tests_set.flac", "artist", value)
            metadata = fetch_metadata(f"{HERE}/flacs/tests_set.flac")
        except Exception as e:
            print(f'Received error: {e}')

        os.remove(dest)
        self.assertEqual(7, len(metadata))
        assert 'artist' not in metadata

if __name__ == '__main__':
    unittest.main()