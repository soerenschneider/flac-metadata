
# set pythonpath
export PYTHONPATH:=$(shell pwd)/flacmetadata

tests: unittest

.PHONY: venv
venv:
	if [ ! -d "venv" ]; then python3 -m venv venv; fi
	venv/bin/pip3 install -r requirements.txt

unittest:
	python3 -m unittest

.PHONY: integrationtests
integrationtests:
	for test in inttests/*.sh; do bash $${test}; done

install:
	sh install.sh
